import styled, {css} from 'styled-components'

export const Button = styled.TouchableOpacity`
  margin: 20px;
  padding: 15px;
  backgroundColor:#ccc;
  marginTop: 1px;
  ${props => !!props.marginTop && css`marginTop: ${props.marginTop}`};
`
